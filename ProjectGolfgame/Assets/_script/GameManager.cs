﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;
    public Vector3 startPoint;
    public Vector3 lastCheckPoint;
    public GameObject completeUI;

    private void Start()
    {
        startPoint = new Vector3(1.3f, 7.6f, 7.8f);
    }
    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void Complete()
    {
        completeUI.SetActive(true);
    }
}
