﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderOfParticle : MonoBehaviour
{
    public ParticleSystem ps;

    void Start()
    {
        
        ps.Stop(true);
    }

    // Update is called once per frame
    
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            //Debug.Log("Play");
            
                ps.Play(true);
            
            
            
            

        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            ps.Stop(true);
        }
    }

}
