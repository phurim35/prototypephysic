﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndTriger : MonoBehaviour
{
    public GameManager gameManager;
    public bool isEnd=false;

    public bool IsEnd
    {
        get
        {
            return isEnd;
        }
        set
        {
            isEnd = value;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        isEnd = true;
        gameManager.Complete();
    }
    private void Update()
    {
        if (isEnd == true)
        {
            if (Input.GetMouseButtonDown(0))
            {
                SceneManager.LoadScene("SampleScene");
            }
        }
    }
    
}
