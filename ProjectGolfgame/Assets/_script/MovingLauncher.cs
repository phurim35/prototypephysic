﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingLauncher : MonoBehaviour
{
    Vector3 MoveX = new Vector3(10f, 0, 0);
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.Translate(MoveX * Time.deltaTime);
        if (this.transform.position.x >= 8)
        {
            this.transform.position = new Vector3(8f, 47.84f, 1678.4f);
            MoveX.x *= -1f;
            
        }
        
        if (this.transform.position.x <= -4)
        {
            this.transform.position = new Vector3(-4f, 47.84f, 1678.4f);
            MoveX.x *= -1f;
            
        }
    }
}
