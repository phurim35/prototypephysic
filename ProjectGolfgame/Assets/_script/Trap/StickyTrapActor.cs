﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickyTrapActor : MonoBehaviour
{
    FixedJoint _fixedJoint = null;
    bool isJointing=false;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (isJointing == true)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Debug.Log("ReleaseFixedJoint");
                ReleaseFixedJoint();
            }
        }
        

    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Sticky"))
        {
            isJointing = true;
            Debug.Log("Click mouse Left!!");
            GameObject go = other.gameObject;
            StickyTrap stickyTrap = go.GetComponent<StickyTrap>();
            Shootingscript.shootingcharge = 2;


            _fixedJoint = this.gameObject.AddComponent<FixedJoint>();
                _fixedJoint.connectedBody = other.gameObject.GetComponent<Rigidbody>();

            
                
            
        }
        

    }
    void OnCollisionExit(Collision other)
    {
        if (other.gameObject.CompareTag("Sticky"))
        {
            isJointing = false;
        }
    }

    public void ReleaseFixedJoint()
    {
        _fixedJoint.connectedBody = null;
        Destroy(_fixedJoint);
    }
}
