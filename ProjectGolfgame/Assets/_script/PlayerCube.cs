﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCube : MonoBehaviour
{
    public GameManager gm;
    public PhysicMaterial physicmat;
    public Text PhysicmatText;
    public GameObject windZone;
    [SerializeField]
    private bool inWindZone = false;
    [SerializeField]
    private Material PlayerMat;
    public bool Isice = false;
    public static bool onGround=false;
    public AudioSource deathAudio;
    

    

    Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("IceChanger"))
        {
            GetComponent<Collider>().material = physicmat;
            Isice = true;
        }
        if (other.gameObject.CompareTag("NormalChanger"))
        {
            GetComponent<Collider>().material = null;
            Isice = false;
        }
        
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            onGround = true;
            
            

        }
        
        
       
        if (collision.gameObject.CompareTag("JumpPad"))
        {
            Shootingscript.shootingcharge = 2;
            Rigidbody rb = this.gameObject.GetComponent<Rigidbody>();
            rb.AddForce(new Vector3(0, 20, 0), ForceMode.Impulse);
        }
        if (collision.gameObject.CompareTag("Spike"))
        {
            Respawn();
        }
    }
    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            onGround = true;
            if (onGround == true)
            {
                Shootingscript.shootingcharge = 2;

            }
        }
            
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            onGround = false;
            Shootingscript.shootingcharge--;
        }
            
        
        
    }

    private void OnTriggerStay(Collider other)
    {
        windZone = other.gameObject;
        if(other.CompareTag("windarea"))
        {
            rb.AddForce(windZone.GetComponent<WindArea>().direction * windZone.GetComponent<WindArea>().strength);
        }
        
    }
    


    void Update()
    {
        if(this.gameObject.transform.position.y < -1)
        {
            deathAudio.Play();

            Respawn();
        }
        if(Isice)
        {
            PhysicmatText.text = "Ice";
            PlayerMat.color = Color.cyan;
        }
        else
        {
            PhysicmatText.text = "None";
            PlayerMat.color = Color.green;
        }
        
        
        

    }

    public void Respawn()
    {
        
        this.gameObject.transform.position = gm.lastCheckPoint;
        Isice = false;
        EndTriger isEnd = new EndTriger();
        isEnd.isEnd = false;
    }


}
