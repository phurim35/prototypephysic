﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatformY : MonoBehaviour
{
    Vector3 MoveY = new Vector3(0, 5f, 0);
    // Update is called once per frame
    void Update()
    {
        transform.Translate(MoveY * Time.deltaTime,Space.World);
        if(this.transform.position.y <= 8 )
        {
            this.transform.position = new Vector3(-6f, 8f, 286f);
            MoveY.y *= -1f;
            
        }
        
            
        if (this.transform.position.y >= 20)
        {
            this.transform.position = new Vector3(-6f, 20f, 286f);
            MoveY.y *= -1f;
        }
        


    }
}
