﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Launcher : MonoBehaviour
{
    [SerializeField]
    float _mass;

    [SerializeField]
    float _forceMagnitude;

    [SerializeField]
    float _luncherInterval;

    

    void Start()
    {
        Invoke("LaunchBall", Random.Range(0.5f, 1f));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void LaunchBall()
    {
        GameObject go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        go.transform.position = this.transform.position;

        Rigidbody rb = go.AddComponent<Rigidbody>();
        
        rb.gameObject.AddComponent<CheckCollision>();
        rb.mass = _mass;
        rb.transform.localScale = new Vector3(1f,1f,1f);
        rb.useGravity = false;
        Collider cd=go.AddComponent<SphereCollider>();
        cd.isTrigger = true;
        rb.AddForce(this.transform.forward * _forceMagnitude*-1, ForceMode.Impulse) ;
        rb.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;

        //Destroy(go,5);

        Invoke("LaunchBall", Random.Range(0.5f, 1f));
    }
    
}
